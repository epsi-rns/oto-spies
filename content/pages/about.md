+++
title = "About"
+++
 
Halo, nama saya Simon.
Pengelola bengkel cat di Cinere, Depok.
Lokasi di daerah selatan, keluar dari Jakarta.

{{< card-image src="posts/2019/02-oven/20190202_154406.jpg" >}}

### Layanan

* Melayani Bengkel cat mobil dengan Oven.
  (Body Repair dan Painting)

* Mengganti atau merubah warna, cat full total body,
  atau pengerjaan cet per panel.

* Perbaikan body, ringan dan berat 
  (Pemulihan lecet, tergores, penyok)

* Kenteng teter (Ketok magic, salon/poles, dan las).

* Bongkar pasang kaca.

{{< card-image src="posts/2019/04-bmw/20190509_095207.jpg" >}}

[//]: <> ( -- -- -- links below -- -- -- )

[image-13]:   {{< baseurl >}}assets/site/images/authors/epsi-13.jpg

[local-blog]: {{< baseurl >}}backend/2014/01/02/content-example/

